/*__________________________FHT CORE SOURCE CODE__________________________
	version 0.22
  _______________________________________________________________________*/
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/dir.h>
#include <math.h>
#ifdef SGI
#include <malloc.h>
#include <dmedia/audiofile.h> 
#else
#include <audiofile.h>
#endif
#include <string.h>
#include <ctype.h>



char realaf_readSound(AFfilehandle infile,double* signal, /*double* zeros,*/ long step , long* position, long frmcnt);
char readSound(double* signal, double* zeros, long step , long* position);
char realreadSound(double* signal, long step , long* position);
FILE* openSound(char* name, long Nw, long windowStep);

static short*	allocFramebuff(long windowSize);
char af_readSound(AFfilehandle infile,double* signal, double* zeros, long FFT_size,
 	long step , long* position, long frmcnt);
int r_shiftin(double A[], int  Nw, int  D,  int samps_per_frame, 
	AFfilehandle infile, int *si_valid);
int rl_shiftin(double A[], int  Nw, int  D,  int samps_per_frame,  int bits_per_samp,
	AFfilehandle infile, int *si_valid);
void fold( double I[],double W[], int Nw, double O[], double Z[],int N, int n );

static short	*buffer	 		= NULL;
static long 	*lbuffer	 	= NULL;
static short	*framebuff	 	= NULL;
static long		buffSize 		= 0;
static FILE*	sndFile			= NULL;


/***********************************************
	openSound
***********************************************/

FILE* openSound(char* name, long windowSize, long windowStep)
{
	buffSize	= windowSize;
	sndFile 	= fopen(name, "r");

	buffer	= (short*) calloc(buffSize, sizeof(short));
	return sndFile;
}


/***********************************************
	readSound
***********************************************/

char readSound(double* signal, double* zeros, long step , long* position)
{
	int i/*, j*/;
	#ifndef LINUX
	fpos_t k/*, header_offset*/;
 	#else
 	unsigned long k;
 	#endif
        k = 0;
        /*
	k += header_offset;
        */ 
	
	
	#ifndef LINUX
	k += (fpos_t)(step*(*position));
	fsetpos( sndFile, &k);
	#else
	k = (unsigned long)(step*(*position));
	fseek( sndfile, step, SEEK_CUR);
	#endif
        /*
        fsetpos( sndFile, ((const fpos_t*)&position));
	*/
	(*position)++;/*(*position)+=channels*/
	
	fread(buffer, sizeof(short), buffSize, sndFile);

	if(feof(sndFile)) return 1;

	for (i=0; i<buffSize; i++)
	{
		signal[i] 	= (double)buffer[i];
		zeros[i]	= 0.0;
	}

	/*return (feof(sndFile)) ? 1 : 0;*/
	if(feof(sndFile)) return 1;
 	return  0;
}	
/***********************************************
	estimateRawSound
************************************************/
int estimateRawSound( long step, long buffsize )
{
	int /*i, */j;
	short /*temp,*/ q;
	short *tbuf/*[step]*/;
	#ifndef LINUX
	fpos_t k/*, header_offset*/;
 	#else
 	unsigned long k;
 	#endif
    tbuf = (short*)calloc(step,sizeof(short));
    q = buffsize/step;

for(j = 0, k = 0;;j++, k++){
	fread(tbuf, sizeof(short),step, sndFile);
	if(j<=q)k=0;
	if(feof(sndFile))break;
	}

	/*if(feof(sndFile))*/
	return k+1;
}	
	
/***********************************************
    open sound using  SGI audiofile library
************************************************/

/***********************************************
	allocate buffer
*************************************************/
void allocBuf(long windowSize, long windowStep)
{
	buffSize	= windowSize;
	buffer	= (short*) calloc(buffSize, sizeof(short));
	lbuffer = (long*) calloc(buffSize, sizeof(long));
}
/***********************************************
    read sound using SGI audio library
************************************************/

char af_readSound(AFfilehandle infile,double* signal, double* zeros, long FFT_size, long step , long* position, long frmcnt)
{
	int i/*, j*/;
	int pos;
	pos = (int)(*position);
	if((long)pos == frmcnt) return 1;
	afSeekFrame(infile, AF_DEFAULT_TRACK, pos);
	pos = afReadFrames(infile, AF_DEFAULT_TRACK, buffer, (int)buffSize);

	(*position) += step;

	for (i=0; i<buffSize; i++)
	{
		signal[i] 	= (double)buffer[i];
		zeros[i]	= 0.0;
	}

	
	return 0;
}
/***********************************************
    read sound using SGI audio library
************************************************/

char realaf_readSound(AFfilehandle infile,double* signal, /*double* zeros,*/ long step , long* position, long frmcnt)
{
	int i/*, j*/;
	int pos;
	pos = (int)(*position);
	if((long)pos == frmcnt) return 1;
	afSeekFrame(infile, AF_DEFAULT_TRACK, pos);
	pos = afReadFrames(infile, AF_DEFAULT_TRACK, buffer, (int)buffSize);

	(*position) += step;

	for (i=0; i<buffSize; i++)
	{
		signal[i] 	= (double)buffer[i];
		/*zeros[i]	= 0.0;*/
	}	
	return 0;
}



/***********************************************
   write sound using SGI audio library
************************************************/
static short* allocFramebuff(long windowSize)
{
	framebuff	= (short*) calloc(2*windowSize, sizeof(short));
	return framebuff;
}

/***********************************************
  shift-in reading using SGI audio library
************************************************/
int r_shiftin(double A[], int  Nw, int  D,  int samps_per_frame, 
	AFfilehandle infile, int *si_valid)
	{
    int i;
    double *Aptr = A;
	int sampsread;
    if (*si_valid<0) (*si_valid)=Nw;
    i = Nw-D;

    while (i-- >0) {
      *Aptr = *(Aptr+D);
      Aptr++;
    }

    if (*si_valid==Nw) {
	  short int *frames = buffer;
      sampsread=AFreadframes(infile, AF_DEFAULT_TRACK, frames, D);

      if (samps_per_frame==1) {
        *si_valid = sampsread+Nw-D;
        while(sampsread-- > 0) *(Aptr++) = (double)(*(frames++)) /*/ 32768.0*/;
      } else {
      /*sampsread /= 2; */
        *si_valid = sampsread+Nw-D;
        while(sampsread-- > 0) *(Aptr++) = (double)(*(frames++)+ *(frames++)) /*/ 65536.0*/;
      }
    }
    if (*si_valid<Nw) {
        for (i= *si_valid; i<Nw; i++) A[i]=0.;
        *si_valid-=D;
    }
    return (*si_valid<=0);
}

int rl_shiftin(double A[], int  Nw, int  D,  int samps_per_frame, 
	int bits_per_samp, AFfilehandle infile, int *si_valid)
	{
    int i;
    double *Aptr = A;
	int sampsread;
    if (*si_valid<0) (*si_valid)=Nw;
    i = Nw-D;

    while (i-- >0) {
      *Aptr = *(Aptr+D);
      Aptr++;
    }

    if (*si_valid==Nw) {
	  long int *lframes = lbuffer;
	  short int *frames =  buffer;

	if(bits_per_samp==16){
      sampsread=AFreadframes(infile, AF_DEFAULT_TRACK, frames, D);
/*	printf("16\n");*/
      if (samps_per_frame==1) {
        *si_valid = sampsread+Nw-D;
        while(sampsread-- > 0) *(Aptr++) = (double)(*(frames ++)) /*/32768.0*/;
      } else {
      /*sampsread /= 2; */
        *si_valid = sampsread+Nw-D;
        while(sampsread-- > 0) *(Aptr++) = (double)(*(frames++)+ *(frames++)) /*/65536.0 */;
      }
	}else if(bits_per_samp==24){
      sampsread=AFreadframes(infile, AF_DEFAULT_TRACK, lframes, D);
/*	printf("24\n");*/
      if (samps_per_frame==1) {
        *si_valid = sampsread+Nw-D;
        while(sampsread-- > 0) *(Aptr++) = (double)(*(lframes++)) /*/8388608.0*/;
      } else {
      /*sampsread /= 2; */
        *si_valid = sampsread+Nw-D;
        while(sampsread-- > 0) *(Aptr++) = (double)(*(lframes++)+ *(lframes++)) /*/16777216.0 */;
      }
	}else{
/*	printf("32\n");*/
      sampsread=AFreadframes(infile, AF_DEFAULT_TRACK, lframes, D);
      if (samps_per_frame==1) {
        *si_valid = sampsread+Nw-D;
        while(sampsread-- > 0) *(Aptr++) = (double)(*(lframes++)) /*/2147483647. */;
      } else {
      /*sampsread /= 2; */
        *si_valid = sampsread+Nw-D;
        while(sampsread-- > 0) *(Aptr++) = (double)(*(lframes++)+ *(lframes++)) /*/4294967294. */;
      }
	}
    }

    if (*si_valid<Nw) {
        for (i= *si_valid; i<Nw; i++) A[i]=0.;
        *si_valid-=D;
    }
    return (*si_valid<=0);
}
/***********************************************
  signal folding and windowing
************************************************/
void fold( double I[],double W[], int Nw, double O[], double Z[],int N, int n )
{
    int i;

    for ( i = 0; i < N; i++ )
        Z[i] = (O[i] = 0.);

    while ( n < 0 )
        n += N;
    n %= N;
    for ( i = 0; i < Nw; i++ ) {
        O[n] += I[i]*W[i];
        if ( ++n == N )
            n = 0;
    }
}
