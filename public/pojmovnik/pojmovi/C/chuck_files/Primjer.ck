// polyphonic example 
// (also see ../midi/polyfony.ck for more polyphony)

// The size of the array should be the max number of instruments
// to play simultaneously, at any point.
Rhodey rhodies[4]; //STK instrument modeled after Rhodes piano
// variable to remember the last one played
int which;

// patch
Gain g => dac;
.4 => g.gain;
// connect the instruments
for( int i; i < rhodies.cap(); i++ )
    rhodies[i] => g;

// Messiaen's Second Mode
[ 58, 59, 61, 62, 64, 65, 67, 68, 70 ] @=> int notes[];

// infinite time-loop
while( true )
{
    for( int i; i < notes.cap(); i++ )
    {         
        Math.random2(0, 8) => int j;
        play( notes[j], Math.random2f( .1, .6 ) );
        300::ms => now;
    }
}

// basic play function (add more arguments as needed)
fun void play( float note, float velocity )
{
    // first figure which to play
    // round robin may work
    ( which + 1 ) % rhodies.cap() => which;

    // start the note
    Std.mtof( note ) => rhodies[which].freq;
    velocity => rhodies[which].noteOn;
}