comb()
{
//...
//a, b, j and amp are parameters
	// FIR comb filter
	delaysamp = ( i > j ? i - j : 0); 
	if (type == FIR)
	  out[0] = ((a * in[i]) + (b * x[delaysamp])) * amp;
	// IIR comb filter 
	else if (type == IIR)
	  out[0] = ((a * in[i]) + (b * y[delaysamp])) * amp;
//...
return;
}