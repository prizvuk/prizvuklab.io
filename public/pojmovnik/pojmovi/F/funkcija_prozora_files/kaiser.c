/*______________Kaiser Window function___________________________________*/
/*_____________float bessel_0 (float kx)_________________________________*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * The following code is a line-by-line C-translation  of the original 
 * FORTRAN code for modified zero-order Bessel function,  by. J.Kaiser 
 * himself (as found in Rabiner & Gold's "DSP cookbook"-Theory and  
 * Application of Digital Signal Processing - ISBN 0-13-914101-4)
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*kx between 0. & 20.*/
float bessel_0(float bx)
{
short i;
float t, de, sde, e, by;
short m;
by=bx/2.0;
t=0.00000001;
e=1.0;
de=1.0;
for(i=1;i<=25;i++)
	{
	de=de*by/(float)i;
	sde=de*de;
	e=e+sde;
	if(e*t-sde>0.0)
	   break;
	}
bx=e;
m=i;
return bx;
}
 /* * * * * * * * * * * * * * * * * *
 *  b= 2.120 ------------->  - 30 dB
 *  b= 4.538 ------------->  - 50 dB
 *  b= 6.764 ------------->  - 70 dB
 *  b= 8.960 ------------->  - 90 dB
 * * * * * * * * * * * * * * * * * */
float kaiser( int j, int n, float b )
{
 float kx, ky;
 float w;
 ky= 2.0*((float)j+.5)/(float)(n);
 kx= b*sqrt(1.0-ky*ky);
 w= bessel_0(kx)/bessel_0(b);
// printf("#%d\t%d\t%f\t%f\n",n,j,kx,w); 
 return w;
}