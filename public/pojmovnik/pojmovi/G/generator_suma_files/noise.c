// based of F.R. Moore's original code for cmusic
// cc -o noise noise.c

#include <stdlib.h>
#include <stdio.h>
#include <math.h>


// fran - return a floating point random number scaled to be in the range [lb,ub] ------ 
float   divisor;
float fran(float lb, float ub)
{
	if (divisor == 0)
		divisor = powf(2.0, (float) ((sizeof(int) * 8) - 1)) - 1.0;
	return ((ub - lb) * (random() / divisor) + lb);
}

// main program - print a series of noise values----------------------------------------
int main (int argc, char *argv[])
{
int i, n, lastn, N, length;
float halfrange, *r, R;

// set default value for n
	N = 4;
	
// use another value if N is specified
	if (argc > 1) 
			N = atoi( argv[1] );
			

// allocate the memory to hold results of individual random generators
	r = malloc( N * sizeof(float) );
	
// compute length of output sequence
	for ( length=1, i=0 ; i < N ; i++ )
			length <<= 1;
			
// normalize halfrange so that sum of all generators lies in range from -1 to 1.
	halfrange = 1./N;
	
// initialize previous index value
	lastn = length - 1;
	
// generate the sequence
	for ( n=0 ; n<length ; n++ ){
	// at each step, check for changing bits and update corresponding random numbers
	// --their sum is the output
		for ( R=i=0 ; i<N ; i++ ){
			if ( ( (1<<i) & n ) != ( (1<<i) & lastn ) )
				r[i] = fran ( -halfrange, halfrange );
				R += r[i];
			}
			printf( "%f\n", R );
			
			lastn = N;	
	}
return 0;	
}