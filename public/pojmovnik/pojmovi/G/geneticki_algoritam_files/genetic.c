// file: genetic.c
//       based on: geneticAlgorithm1.c
//       by: John LeFlohic, Stanford University, February 24, 1999
//       use: simulates a species' convergence to a model
//       maps results of n evolutionary runs into a 12-tone melody
// adapted for musical demonstration by: Stanko Juzbašić
//
//       cc -o genetic genetic.c

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include <math.h>

//genetic definitions
//_________________________________________________________________
#define NUMBER_ORGANISMS 100
#define NUMBER_GENES     20
#define ALLELES          4
#define MUTATION_RATE    0.001

#define MAXIMUM_FITNESS NUMBER_GENES
#define FALSE 0
#define TRUE  1

// equal temperament definitions
//_________________________________________________________________
#define etm6 1.5874010519681994748
#define etM6 1.6817928305074290861
#define etm7 1.7817974362806786095


// global variables
//_________________________________________________________________
char **currentGeneration, **nextGeneration;
char *modelOrganism;
int  *organismsFitnesses;
int  totalOfFitnesses;


// function declarations
//_________________________________________________________________
void AllocateMemory(void);
int  DoOneRun(void);
void InitializeOrganisms(void);
int  EvaluateOrganisms(void);
void ProduceNextGeneration(void);
int  SelectOneOrganism(void);

// functions
//_________________________________________________________________
int main(int argc, char *argv[]){
  
    int pitch_classes = 12; //0-11
    int ref_oct = 5;        //5-th octave
    float A = 440.;           
    int   midA = 69;
    float Cf = A/etM6;      //middle C  
    int refC = ref_oct*pitch_classes;
    int offs = refC - midA;
    
  int finalGeneration;  
    
  int n = (argc > 1 ? atoi(argv[1]) : 12);  
  if( n < 1 )
     return 1;
     
  AllocateMemory();
  for( int i = 0; i < n; i++){
      finalGeneration = DoOneRun();
      //printf("The final generation was: %d\n", finalGeneration%12);
      //printf("%d\t", finalGeneration);
      int temp = (int)(finalGeneration - 1)%pitch_classes;
      int midi = offs + temp;
      float freq = powf(2., midi/(float)pitch_classes)*A;
      printf("%7.2f\t", freq);
  }
  printf("\n");
  return 0;
}

//_________________________________________________________________
void AllocateMemory(void){
  int organism;

  currentGeneration =
    (char**)malloc(sizeof(char*) * NUMBER_ORGANISMS);
  nextGeneration =
    (char**)malloc(sizeof(char*) * NUMBER_ORGANISMS);
  modelOrganism = 
    (char*)malloc(sizeof(char) * NUMBER_GENES);
  organismsFitnesses = 
    (int*)malloc(sizeof(int) * NUMBER_ORGANISMS);

  for(organism=0; organism<NUMBER_ORGANISMS; ++organism){
    currentGeneration[organism] =
      (char*)malloc(sizeof(char) * NUMBER_GENES);
    nextGeneration[organism] =
      (char*)malloc(sizeof(char) * NUMBER_GENES);
  }
}

//_________________________________________________________________
int DoOneRun(void){
  int generations = 1;
  int perfectGeneration = FALSE;

  InitializeOrganisms();

  while(TRUE){
    perfectGeneration = EvaluateOrganisms();
    if( perfectGeneration==TRUE ) return generations;
    ProduceNextGeneration();
    ++generations;
  }
}

//_________________________________________________________________
void InitializeOrganisms(void){
  int organism;
  int gene;

  // initialize the normal organisms
  for(organism=0; organism<NUMBER_ORGANISMS; ++organism){
    for(gene=0; gene<NUMBER_GENES; ++gene){
      currentGeneration[organism][gene] = rand()%ALLELES;
    }
  }

  // initialize the model organism
  for(gene=0; gene<NUMBER_GENES; ++gene){
    modelOrganism[gene] = rand()%ALLELES;
  }
}

//_________________________________________________________________
int EvaluateOrganisms(void){
  int organism;
  int gene;
  int currentOrganismsFitnessTally;

  totalOfFitnesses = 0;

  for(organism=0; organism<NUMBER_ORGANISMS; ++organism){
    currentOrganismsFitnessTally = 0;

    // tally up the current organism's fitness
    for(gene=0; gene<NUMBER_GENES; ++gene){
      if( currentGeneration[organism][gene]
          == modelOrganism[gene] ){
        ++currentOrganismsFitnessTally;
      }
    }

    // save the tally in the fitnesses data structure
    // and add its fitness to the generation's total
    organismsFitnesses[organism] =
      currentOrganismsFitnessTally;
    totalOfFitnesses += currentOrganismsFitnessTally;

    // check if we have a perfect generation
    if( currentOrganismsFitnessTally == MAXIMUM_FITNESS ){
      return TRUE;
    }
  }
  return FALSE;
}

//_________________________________________________________________
void ProduceNextGeneration(void){
  int organism;
  int gene;
  int parentOne;
  int parentTwo;
  int crossoverPoint;
  int mutateThisGene;

  // fill the nextGeneration data structure with the
  // children
  struct timeval t1;
  gettimeofday(&t1, NULL);
  srand(t1.tv_usec * t1.tv_sec);
  
  for(organism=0; organism<NUMBER_ORGANISMS; ++organism){
    parentOne = SelectOneOrganism();
    parentTwo = SelectOneOrganism();
    crossoverPoint  =  rand() % NUMBER_GENES;

    for(gene=0; gene<NUMBER_GENES; ++gene){

      // copy over a single gene
      mutateThisGene = rand() % (int)(1.0 / MUTATION_RATE);
      if(mutateThisGene == 0){

        // we decided to make this gene a mutation
        nextGeneration[organism][gene] = rand() % ALLELES;
      } else {
        // we decided to copy this gene from a parent
        if (gene < crossoverPoint){
          nextGeneration[organism][gene] =
            currentGeneration[parentOne][gene];
        } else {
          nextGeneration[organism][gene] =
            currentGeneration[parentTwo][gene];
        }
      }
    }
  }

  // copy the children in nextGeneration into
  // currentGeneration
  for(organism=0; organism<NUMBER_ORGANISMS; ++organism){
    for(gene=0; gene<NUMBER_GENES; ++gene){
      currentGeneration[organism][gene] = 
        nextGeneration[organism][gene];
    }
  }
}

//_________________________________________________________________
int SelectOneOrganism(void){
  int organism;
  int runningTotal;
  int randomSelectPoint;

  runningTotal = 0;
  randomSelectPoint = rand() % (totalOfFitnesses + 1);

  for(organism=0; organism<NUMBER_ORGANISMS; ++organism){
    runningTotal += organismsFitnesses[organism];
    if(runningTotal >= randomSelectPoint) 
        return organism;
  }
}
