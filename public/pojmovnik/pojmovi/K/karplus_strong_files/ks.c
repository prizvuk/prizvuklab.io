//  Karplus-Strong algorithm - plucked string
//  simple unix demo program by prizvuk.hr
//  cc -o ks ks.c

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
 
  int R = argc>3 ? atoi(argv[3]) : 44100; //rate
  int T = argc>2 ? atoi(argv[2]) : 10;    //duration
  int f = argc>1 ? atoi(argv[1]) : 261;   //frequency (Hz)
  int N = R / f;                          //table size
  float t[N];                             //table (in C11)
  
  for (int i = 0 ; i < N ; i++ )
     t[i] = 2.*(rand()/(float)RAND_MAX - .5);
     
  int bh = 0;

  for (int i = T*R ; i > 0 ; i--) {
     printf("%f\n",t[bh]);
     int nbh = (bh != N-1 ? bh+1 : 0);
     float avg = .999 * .5 * (t[bh] + t[nbh]);
     t[bh] = avg;
     bh = nbh;
  }
  return 0;
}