//________________________________________________________________________________________
//  Frequency modulation by means of envelope-followed phase modulator
//________________________________________________________________________________________
void pmBuffer(float carrf, float pmindx, float efreq, long fbuffersize)
{

    typedef struct envelope{
        long count;
        Float32 val;
        long step;
    } env;
    
    long numstep;
    long step;

    Float32 result;
    Float32 phase;
    env *envBuf;
    Float32* abuf;
    
    Float32 rad = (Float32)(carrf*TWOPI/R);
    
    //envelope extractor   
    step = (long)(R/efreq);
    numstep = fBuffersize/step + 1;
    envBuf = (env*)calloc(numstep, sizeof(env));
    long k = 0;
    for(long i= 0 ; i < fBuffersize; i += step){
        long j = i;
        Float32 sum = 0.;
        for(; j < i+step; j++){
            if(j >= (fBuffersize - 1)) sum = 0;
            else sum += (fBuffer[j]*fBuffer[j]);
        }
        sum = sqrtf(sum)/(Float32)(j - i);
        envBuf[k].count = i;
        envBuf[k].val = sum;
        if((j - i + 1 ) == step)envBuf[i].step = step;
        else envBuf[k].step = (j - i +1);
        k++;
    }
    
    Float32 max = 0.;
    for(long j = 0; j < numstep; j++)
        if(envBuf[j].val > max) max = envBuf[j].val;
        
    for(long j = 0; j < numstep; j++){
        envBuf[j].val = envBuf[j].val*.5/max;
        if(dbp)printf("envBuf[%ld].count = %ld\tenvBuf[%ld].val = %f\n",j, envBuf[j].count, j, envBuf[j].val);
    }
    
    abuf = (Float32*)calloc(fBuffersize, sizeof(Float32));
    
    for(long i= 0; i < numstep; i ++){
        long j = envBuf[i].count;
        for(; j < envBuf[i+1].count; j++){
            long incr = j - envBuf[i].count;
            abuf[j] = envBuf[i].val + ((Float32)incr*(envBuf[i+1].val - envBuf[i].val))/(envBuf[i+1].count - envBuf[i].count);
            
            if(j >= fBuffersize){
                goto modul;
            }
        }
    }   
    
modul:
    //phase modulator
    for(long i= 0; i < fBuffersize; i++){
        phase = i*rad ;
        phase += pmindx*fBuffer[i];
        if(phase > TWOPI)phase -= TWOPI;
        result = sinf(phase);
        //multiply by linear interpolated envelope value
        
        fBuffer[i] = result*abuf[i];
    }
    
    free((Float32*)abuf);
    free((env*)envBuf);
    return;
}