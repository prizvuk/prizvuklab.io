// primjer rekurzije, funkcija za izračun Fibonaccijevog niza u jeziku C
//______________________________________________________________________
long long int fib(unsigned long long num, unsigned long long val , unsigned long long prev )
{
 if(num == 0) return prev;
 if(num == 1) return val;
 return fib(num - 1, val+prev, val);
}