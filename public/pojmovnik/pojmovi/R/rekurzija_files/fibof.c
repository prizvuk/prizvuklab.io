//file: fibof.c
//computes fibonacci % 12 - periodicity = 24.
// cc -o fibof fibof.c
//
// computes the row on middle C
// 2015, Stanko Juzbasic, published under terms of GPL
//__________________________________________________________
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>

#define etM6 1.6817928305074290861

float A;

int pitch_classes = 12; //0-11

int ref_oct = 5;        //referent octave: 5-th octave

int pca[2000];
//__________________________________________________________________________________________
long long int fib(unsigned long long num, unsigned long long val , unsigned long long prev )
{
 if(num == 0) return prev;
 if(num == 1) return val;
 return fib(num - 1, val+prev, val);
}
//__________________________________________________________________________________________
void usage (void){
printf("USAGE:\n\tfibof n [-]\n\tn is an integer\n\t- compute only the row on C\n\n");
return;
}
//__________________________________________________________________________________________
//returns true if so:
int isNumber(char number[])
{
int i = 0;
if (number[0] == '-') i = 1;
    
for (; number[i] != 0; i++){
   if (!isdigit(number[i])) return 0;
 }
return 1;
}
//__________________________________________________________________________________________
int main (int argc, char* argv[])
{
A          = 440.;    //tuning A
int   midA = 69;
float   Cf = A/etM6;  //middle C
int   refC = 60;
int   offs = refC - midA;
int      m = 1;
    
    
if(argc < 2 || ((argc > 2)&&(argv[1] != '-')){
u:
  usage();
  return 1;
  }

if (!isNumber(argv[1]))goto u;
int n = atoi(argv[1]) - 1; //periodicity (0-23 == 24)

if ( n <= 0 ) n = 0;

for(int i = 0; i <= n; i++){
      pca[i] = fib((unsigned long long)i, 1, 0)%pitch_classes;
  }
   
if(argc == 2) m = n;
    
for(int j = 0; j <= m; j++){
	//rectus
    for(int i = 0; i <= n; i++){
      int midi = offs + pca[i] + pca[j];
      if(0) printf("%d\n",midi);
      float freq = powf(2., midi/(float)pitch_classes)*A;
      printf("%7.2f\t", freq);
    }

    //retro
    for(int i = n; i >= 0; i--){
      int midi = offs + pca[i] - pca[j];
      float freq = powf(2.,(midi)/(float)pitch_classes)*A;
      printf("%7.2f\t", freq);
    }

    //inversus
    for(int i = 0; i <= n; i++){
      int midi = offs + pitch_classes - pca[i] + pca[j];
      float freq = powf(2.,(midi)/(float)pitch_classes)*A;
      printf("%7.2f\t", freq);
    }  
    //retro inversus
    for(int i = n; i >= 0; i--){
      int midi = offs + pitch_classes - pca[i] + pca[j];
      float freq = powf(2.,(midi)/(float)pitch_classes)*A;
      printf("%7.2f\t", freq);
   
    }          
    
printf("\n");
}
return 0;
}