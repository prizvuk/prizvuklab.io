//TipueSearch has more flexibilities in searching mode, search sets, related search and other output options.
//Below is the template to generate the “tipuesearch_content.js” file (by changing name of resulting tipuesearch/index.html)

var tipuesearch = {"pages": 
[{
	 { range $index, $page := .Site.Pages }}{{ if eq $page.Type "post" }}{{ if $index }},{{ end }}
     {"title": "{{ $page.Title }}",  "text": "{{ $page.Params.description }}", 
      "tags": "{{ range $tindex, $tag := $page.Params.tags }}{{ if $tindex }} {{ end }}{{ $tag }}{{ end }}", 
      "url": "{{ .Site.BaseURL }}{{ $page.RelPermalink }}"}{{ end }}{{ end }}
]};