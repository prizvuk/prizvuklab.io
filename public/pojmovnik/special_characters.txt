<!-- special html characters -->

<!ENTITY Thorn "&#222;"> 
<!ENTITY thorn "&#254;"> 
<!ENTITY Eth "&#208;"> 
<!ENTITY eth "&#240;"> 
<!ENTITY Aesc "&#198;"> 
<!ENTITY aesc "&#230;"> 
<!ENTITY Amacron "&#256;"> 
<!ENTITY Emacron "&#274;"> 
<!ENTITY Imacron "&#298;"> 
<!ENTITY Omacron "&#332;"> 
<!ENTITY Umacron "&#362;"> 
<!ENTITY Ymacron "&#562;"> 
<!ENTITY AEmacron "&#508;"> 
<!ENTITY amacron "&#257;"> 
<!ENTITY emacron "&#275;"> 
<!ENTITY imacron "&#299;"> 
<!ENTITY omacron "&#333;"> 
<!ENTITY umacron "&#363;"> 
<!ENTITY ymacron "&#563;"> 
<!ENTITY aemacron "&#509;"> 
<!ENTITY nbsp "&#160;"> 
<!ENTITY copy "&#169;"> 
<!ENTITY mdash "&#8212;"> 
<!ENTITY ldquo "&#8220;"> 
<!ENTITY rdquo "&#8221;">